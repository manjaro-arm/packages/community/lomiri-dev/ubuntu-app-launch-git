# Maintainer: Ivan Semkin (ivan at semkin dot ru)

pkgbase=lomiri-app-launch
pkgname=(lomiri-app-launch-git)
_pkgname=lomiri-app-launch
pkgver=r3086.0.0.90.r40.ga2c78e6
pkgrel=1
pkgdesc='Session init system job for Launching Applications'
url='https://github.com/ubports/ubuntu-app-launch'
arch=(x86_64 i686 armv7h aarch64)
license=(GPL)
depends=(zeitgeist gobject-introspection lttng-ust lomiri-api-git properties-cpp curl click-git)
makedepends=(git cmake  cmake-extras-git dbus-test-runner gmock lcov gcovr)
source=('git+https://gitlab.com/ubports/development/core/lomiri-app-launch.git'
        '0001-Add-missing-array-includes.patch')
sha256sums=('SKIP'
            '5ed730f06c89df07da34ba66c3570b7a0fae70d8a24015d13849a0d00b070f0f')

BUILDENV+=('!check') # Some tests are broken

prepare() {
    cd ${_pkgname}
    for i in "${srcdir}/"*.patch; do
        patch -Np1 -i "$i"
    done
}

pkgver() {
  cd ${_pkgname}
  echo "r$(git rev-list --count HEAD).$(git describe --always)" | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

build() {
  cd ${_pkgname}
  sed -i 's/-std=gnu99/-std=gnu99 -Wno-deprecated-declarations/g' CMakeLists.txt
  sed -i 's/-std=c++11/-std=c++11 -Wno-deprecated-declarations/g' CMakeLists.txt
  cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_INSTALL_LIBDIR="lib/" -DCMAKE_INSTALL_LIBEXECDIR="lib/" .
  make
}

check() {
  cd ${_pkgname}
  make ARGS+="--output-on-failure" test
}

package() {
  cd ${_pkgname}
  make DESTDIR="${pkgdir}/" install
}

# vim:set ts=2 sw=2 et:
